data "gitlab_user" "main-owner" {
  username = "emctl"
}

#data "gitlab_user" "dev1" {
#  username = ""
#}

resource "gitlab_group_membership" "main-owner" {
  group_id     = data.gitlab_group.gitops-demo.id
  user_id      = data.gitlab_user.main-owner.id
  access_level = "owner"
}


#resource "gitlab_group_membership" "dev1" {
#  group_id     = data.gitlab_group.gitops-demo.id
#  user_id      = data.gitlab_user.dev1.id
#  access_level = "developer"
#}
